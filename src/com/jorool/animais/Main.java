package com.jorool.animais;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRootPane;

public class Main {

	private static JFrame frame = new JFrame("Jogo dos Animais");

	public static void main(String[] args) {
        iniciar(Jogo.novoJogo());
	}

    private static void iniciar(final Jogo jogo) {
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel label = new JLabel("Pense em um animal!");
        frame.getContentPane().add(label, BorderLayout.CENTER);

        JButton botao = new JButton("OK");
        botao.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jogo.jogar();
            }
        });

        frame.getContentPane().add(botao, BorderLayout.PAGE_END);
        frame.setSize(300, 150);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        JRootPane rootPane = frame.getRootPane();
        rootPane.setDefaultButton(botao);
    }

}
