package com.jorool.animais;

import javax.swing.*;

public class Jogo {

    private Noh raiz;

    private Jogo() {
    }

    private Jogo(Noh raiz) {
        this.raiz = raiz;
    }

    public static Jogo novoJogo() {
        return new Jogo(construirRaizPadrao());
    }

    public void jogar() {
        jogar(raiz);
    }

    private void jogar(Noh noh) {
        if (noh.temAlgumFilho()) {
            if (fazerPergunta(noh)) {
                jogar(noh.getDireita());
            } else {
                jogar(noh.getEsquerda());
            }
        } else {
            fazerPergunta(noh);
        }
    }

    private boolean fazerPergunta(Noh noh) {
        int resposta = JOptionPane.showConfirmDialog(null, noh.getPergunta(), "Jogo dos Animais", JOptionPane.YES_NO_OPTION);

        if (resposta == 0) { //sim
            if (!noh.temAlgumFilho()) {
                acertou();
            }
            return true;
        } else {
            if (!noh.temAlgumFilho()) {
                posicionarNovoNoh(noh);
            }
        }
        return false;
    }

    private void posicionarNovoNoh(Noh nohAtual) {
        Noh novoNoh = perguntarAnimal(nohAtual);
        Noh nohPai = nohAtual.getPai();
        nohAtual.setPai(novoNoh);
        if (nohPai.getEsquerda() == nohAtual) {
            nohPai.setEsquerda(novoNoh);
        } else {
            nohPai.setDireita(novoNoh);
        }
    }

    private Noh perguntarAnimal(Noh nohAtual) {
        String novoAnimal = JOptionPane.showInputDialog(null, "Qual foi o animal que você pensou? ");
        String animalAnterior = nohAtual.getPergunta().split("\\ ")[6];
        String novaAcao = JOptionPane.showInputDialog(null, String.format("Um(a) %s ___________ mas um(a) %s não.", novoAnimal, animalAnterior));

        return contruirNovoNoh(nohAtual, novoAnimal, novaAcao);
    }

    private Noh contruirNovoNoh(Noh nohAtual, String novoAnimal, String novaAcao) {
        Noh novoNoh = new Noh(String.format("O animal que você pensou %s ?", novaAcao));
        Noh novoNohDireita = new Noh(String.format("O animal que você pensou é %s ?", novoAnimal));
        novoNohDireita.setPai(novoNoh);
        novoNoh.setDireita(novoNohDireita);
        novoNoh.setEsquerda(nohAtual);
        novoNoh.setPai(nohAtual.getPai());
        return novoNoh;
    }

    private void acertou() {
        JOptionPane.showMessageDialog(null, "Acertei!");
    }

    private static Noh construirRaizPadrao() {
        Noh raiz = new Noh("O animal que você pensou vive na água ?");
        Noh direita = new Noh("O animal que você pensou é tubarão ?");
        Noh esquerda = new Noh("O animal que você pensou é macaco ?");
        direita.setPai(raiz);
        esquerda.setPai(raiz);
        raiz.setDireita(direita);
        raiz.setEsquerda(esquerda);

        return raiz;
    }

}
