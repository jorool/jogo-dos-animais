package com.jorool.animais;

public class Noh {

    private Noh pai;

    private Noh direita;

    private Noh esquerda;

    private String pergunta;

    public Noh(String pergunta) {
        this.pergunta = pergunta;
    }

    @Override
    public String toString() {
        return this.pergunta;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Noh) {
            Noh o = (Noh) obj;
            return this.pergunta.equals(o.pergunta);
        }
        return false;
    }

    public boolean temAlgumFilho() {
        return this.direita != null || this.esquerda != null;
    }

    public Noh getPai() {
        return pai;
    }

    public void setPai(Noh pai) {
        this.pai = pai;
    }

    public Noh getEsquerda() {
        return esquerda;
    }

    public void setEsquerda(Noh esquerda) {
        this.esquerda = esquerda;
    }

    public String getPergunta() {
        return pergunta;
    }

    public void setPergunta(String pergunta) {
        this.pergunta = pergunta;
    }

    public Noh getDireita() {
        return direita;
    }

    public void setDireita(Noh direita) {
        this.direita = direita;
    }

}
